#include <Arduino.h>

#include "pitches.h"

const int latchPin = 3;
const int clockPin = 0;
const int dataPin = 2;

const int speakerPin = 4;

const int P1 = 5;
const int P2 = 6;
const int P3 = 7;

const int G1 = 1;
const int G2 = 2;
const int G3 = 3;

struct {
  int ground;
  int positive;
} leds[9];

void setLed(int id, int ground, int positive) {
  leds[id].ground = ground;
  leds[id].positive = positive;
}

void setup() {

  pinMode(latchPin, OUTPUT);
  pinMode(dataPin, OUTPUT);  
  pinMode(clockPin, OUTPUT);

  setLed(0, G1, P1);
  setLed(1, G1, P2);
  setLed(2, G1, P3);
  setLed(3, G2, P1);
  setLed(4, G2, P2);
  setLed(5, G2, P3);
  setLed(6, G3, P1);
  setLed(7, G3, P2);
  setLed(8, G3, P3);
}

void updateShiftRegister(byte pattern)
{
   digitalWrite(latchPin, LOW);
   shiftOut(dataPin, clockPin, MSBFIRST, pattern);
   digitalWrite(latchPin, HIGH);
}

void blink(int led, int d) {
  byte pattern = 0;

  bitSet(pattern, G1);
  bitSet(pattern, G2);
  bitSet(pattern, G3);

  bitSet(pattern, leds[led].positive);
  bitClear(pattern, leds[led].ground);

  updateShiftRegister(pattern);

  delay(d);

  pattern = 0;
  bitSet(pattern, G1);
  bitSet(pattern, G2);
  bitSet(pattern, G3);

  updateShiftRegister(pattern);
}

void blinkSequence(int sequence[], int length, int pauseLights, int pauseNoise) {
  for(int i=0; i<length; i++) {
    tone(speakerPin, NOTE_A3, pauseNoise);
    blink(sequence[i], pauseLights);
  }
}

int circle_sequence_a[] = {0,1,2,5,8,7,6,3};
int circle_sequence_b[] = {8,7,6,3,0,1,2,5};
int circle_sequence_c[] = {2,5,8,7,6,3,0,1};
int circle_sequence_d[] = {6,3,0,1,2,5,8,7};
void circle() {
  blinkSequence(circle_sequence_a, 8, 200, 100);
}

void flash_all(int delay) {
  int reps = delay/9;
  for(int t=0; t<reps; t++) {
    for(int l=0; l<9; l++) {
      blink(l, 1);
    }
  }
}

void flash(int ls[], int size, int delay) {
  int reps = delay/size;
  for(int t=0; t<reps; t++) {
    for(int i=0; i<size; i++) {
      blink(ls[i], 1);
    }
  }
}

void two_circle() {
  for(int i=0; i<8; i++) {
    int seq[] = {circle_sequence_a[i], circle_sequence_b[i]};
    tone(speakerPin, NOTE_C4, 100);
    flash(seq, 2, 200);
  }
}

void four_circle() {
  for(int i=0; i<8; i++) {
    int seq[] = {circle_sequence_a[i], circle_sequence_b[i], circle_sequence_c[i], circle_sequence_d[i]};
    tone(speakerPin, NOTE_B3, 100);
    flash(seq, 4, 200);
  }
}

void loop() {
  circle();
  circle();
  circle();
  circle();
  circle();

  delay(500);
 
  two_circle();
  two_circle();
  two_circle();

  delay(500);

  four_circle();
  four_circle();
  four_circle();

  delay(500);

  tone(speakerPin, NOTE_C4, 100);
  blink(4, 200);
  delay(200);
  tone(speakerPin, NOTE_C4, 100);
  blink(4, 200);
  delay(200);
  tone(speakerPin, NOTE_C4, 100);
  blink(4, 200);
  delay(200);

  delay(500);
}

